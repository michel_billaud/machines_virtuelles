% Organisation pour la préparation de machines virtuelles
%
%



# Contexte

Si le boulot consiste à fournir des machines virtuelles Linux diverses
de façon régulière, on a tout intérêt à s'organiser pour travailler de
manière efficace, vu l'aspect répétitif de la chose (il y a souvent
des loupés).

On suppose qu'une machine virtuelle de base a été construite à partir
de la procédure d'installation de la distribution.  Il reste à
installer/configurer le reste, paquets de la distribution, paquets
externes, logiciels à installer "à la main".


D'où des sources d'inefficacité quand on prépare une machine virtuelle

- avoir à entrer des commandes longues comme le bras pour installer
  des choses (fastidieux, risque élevé d'erreurs)
- avoir à télécharger pour la n-ieme fois des archives des logiciels à
  télécharger (lenteur réseau).

Une solution est

- d'avoir des scripts pour les commandes d'installation/configuration
à exécuter par les machines vituelles en cours d'installation
- de partager les scripts, et les archives, entre la machine hôte et
  les machines virtuelles.

# Partager un répertoire avec une machine hôte

Ca n'a rien de compliqué, en installant `sshfs`.

1. Installer `sshfs` sur la machine virtuelle : `apt-get install sshfs`
2. La machine hôte est visible comme passerelle de la machine virtuelle,
faire `ip route` pour connaitre son  numéro IP. Ici 10.0.2.2

~~~
$ ip route
default via 10.0.2.2 ....
....
~~~

3. créer un point de montage, et y monter le répertoire commun

~~~
$ mkdir /tmp/a
$ sshfs billaud@10.0.2.2:Essais/Repertoire  /tmp/a
~~~

Le répertoire `~billaud/Essais/Repertoire` de la machine hôte, après
authentification par `ssh`.

# Organisation

Ce répertoire contiendra

- des archives, téléchargés préalablement.
- les scripts d'installation


# Conclusion

Pour construire une machine virtuelle (après la phase d'installation
de la distribution), on peut s'en sortir avec quelques
commandes manuelles seulement :

- installation de `sshfs`
- montage du répertoire partagé
- lancement d'un script principal dans ce répertoire.



