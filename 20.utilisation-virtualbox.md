# Téléchargement d'une machine virtuelle


todo

# Import sous Virtual box


- Menu : Fichier, Importer un appareil virtuel depuis le Local File System

- Fichier : naviguer jusqu'au fichier ".ova", **Ouvrir**,  **Suivant**
- On peut changer le nom, la taille mémoire etc.
- **Importer**

La machine virtuelle est créée, en extrayant le contenu du fichier OVA
et en appliquant les modifications demandées.

C'est prêt à lancer.
