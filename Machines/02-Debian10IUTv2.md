# La machine "Debian10IUTv2" 

Objectif : cette machine virtuelle, avec un disque assez gros (20 Go),
est utilisable telle quelle pour apprendre à utiliser Linux depuis
Windows, mais avec peu de logiciels installés.

Elle est fournie sous forme d'un fichier `Debian10IUTv2.ova` (Virtual
Appliance) de 2,3 Go installable par VirtualBox[^1].

Elle est basée sur la distribution Debian 10, avec le gestionnaire de bureau 
"mate". Elle n'occupe pas réellement 20 Go sur le disque de la machine hôte,
tant qu'on n'y charge pas plein de choses.


[^1]: et peut être VmWare, non testé. 

## Descriptif

- Mémoire 2 Go.
- Disque virtuel de 20Go 
- partition racine 18 Go, 3,8 Go installés, 13 Go disponibles.

**Logiciels installés** : pendant l'installation de la distribution Debian 10,
on a sélectionné uniquement 

- Desktop Mate 
- Utilitaires système

puis ajouté 

- ssh

**Comptes déclarés** :

| rôle               | identifiant | mot de passe |
| ---                | ---         | ---          |
| administrateur     | `root`      | `root`       |
| utilisateur normal | `guest`     | `guest`      |



## Comment l'obtenir

Le fichier `Debian10IUTv2.ova` peut être obtenu
en suivant le lien
https://filesender.renater.fr/?s=download&token=059b643e-8c93-4013-b4a7-fcbcc3c7b64e

- taille téléchargement = 2,3 Go
- expiration prévue : 22 octobre 2020

Le fichier contient un "Appareil Virtuel" (VA = Virtual appliance),
C'est une archive qui contient un "modèle" pour créer une ou plusieurs
des machines virtuelles que VirtualBox fera tourner.


## Téléchargement de VirtualBox

Pour utiliser les machines virtuelles, installez d'abord le
logiciel VirtualBox, que vous trouverez dans
https://www.virtualbox.org/wiki/Downloads


# Installation et premiers pas

Les étapes suivantes se font sous VirtualBox.

## Création de la machine virtuelle

Dans cette étape, vous allez créer une machine virtuelle à partir
du fichier `Debian10IUTv2.ova` que vous venez de charger.

1. Dans le menu, faire `Fichier > importer un appareil virtuel`[^3]
2. Naviguez jusqu'au fichier `Debian10IUTv2.ova`, et sélectionnez-le.
3. Dans l'écran suivant, vous pouvez éditer le nom (double-clic dessus).
4. En bas à droite, cliquez sur `Importer`[^4]

Sélectionnez le fichier 
[^3]:, ou raccourci clavier  `controle-I`. 
[^4]: Environ une minute, test fait avec le fichier `Debian10IUTv2.ova`
stocké sur une carte SD.


## Démarrage / arrêt de la machine virtuelle


Dans le panneau de gauche de l'interface graphique de VirtualBox, vous
voyez le nom de la machine virtuelle.

**Pour la démarrer**, double-cliquez sur le nom.

Vous devez voir successivement (ne touchez à rien)

- le démarrage du PC virtuel, avec le message "`Press F12 to select
  boot device`",
- le menu de démarrage "GRUB" (choix démarrage linux / options avancées). 
- quelques messages de démarrage du noyau linux,
- la bannière d'authentification graphique.

Un message de VirtualBox s'affiche en haut de la fenêtre. Il dit que,
quand **votre clavier est confisqué** par la machine
virtuelle, vous le libèrerez par la touche **contrôle de
droite**. C'est rappelé en bas de la fenêtre. Fermez ce message.

**Pour arrêter la machine proprement**, plusieurs solutions

- au niveau de la bannière d'authentification clic sur icône on/off en
  haut à droite, choix `Éteindre`.
- depuis une session utilisateur (Mate), menu `Système` / `Éteindre`.

Sinon,

- en simulant l'appui sur le bouton d'alimentation : Menu de la
  fenêtre, `Machine > Extinction par ACPI`
- dans l'interface principale VirtualBox, clic-droit sur
  le nom. Le sous-menu `Fermer` permet :
	  - l'hibernation, en sauvegardant l'état de la machine
	  - l'extinction par ACPI
	  - l'arrêt brutal.

## Adaptation de la taille de l'écran

Vous avez ouvert une session, vous souhaiteriez simuler un écran plus grand

Dans le menu, allez dans `Système`, `Centre de contrôle`, `Affichage`,
et changez la résolution.


# Administration de base

Conseils habituels :

- N'utilisez le compte `root` que quand vous avez absolument besoin
des privilèges.  Pas pour vos TPs de programmation. Tâches
d'administration uniquement.
- Le mot de passe *provisoire* est `root`. Changez-le rapidement pour
  un mot de passe robuste (et ne l'oubliez pas).


## Installer des logiciels

Depuis une session ouverte par `root`.

Par l'interface graphique : Menu `Système` / `Administration` /
`Gestionnaire de paquets Synaptic`.

Si vous savez exactement quel paquet vous voulez installer, ça ira
plus vite en ligne de commande, en tapant dans un terminal (menu
`Applications` `Outils système` / `Terminal MATE` :

~~~
apt-get install xbill
~~~

**Mises à jour**

- de la liste des logiciels disponibles : `apt-get update`
- mise à niveau des logiciels installés : `apt-get upgrade`


**Ménage** : les installations et mises à jour de logiciels
téléchargent des archives qui prennent de la place, on s'en débarasse
par `apt-get clean`.



# Partager des documents avec l'IUT

En tant qu'utilisateur, vous voulez travailler facilement avec vos
fichiers qui se trouvent à l'IUT.

Une possibilité est de faire un **montage** de votre répertoire
personnel distant (celui de l'IUT), qui apparaitra comme un répertoire
de votre machine virtuelle.

**Préparation :**

- Comme administrateur, installez (voir plus haut) le paquet `sshfs`
- Comme utilisateur, créez un dossier vide `IUT` sur le bureau.

**Montage :**

- tapez la commande `sshfs  moncompte@info-ssh1.iut.u-bordeaux.fr  Bureau/IUT`
ls	
- mot de passe, etc.
- En cliquant sur l'icône IUT de votre bureau, vous avez accès à vos fichiers.


**Le démontage** se fait par : `fusermount -u /home/guest/Bureau/IUT`
