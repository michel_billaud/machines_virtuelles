# Objectif

La machine "Debian19IUTv1", qui doit son numéro bizarre à une faute de frappe,
est un premier test de faisabilité.


# Descriptif

Disque virtuel de 8Go 

Logiciels installés

- Debian 10
- Desktop Mate + dépendances (libreoffice, gimp, firefox) 
- Visual Studio Code
- gcc

Téléchargement (2.5 Go) :

- https://filesender.renater.fr/?s=download&token=795086d1-1953-4bf7-8146-d4f14c4bb347

# Utilisation

Deux comptes sont déclarés : 

| rôle               | identifiant | mot de passe |
| ---                | ---         | ---          |
| administrateur     | `root`,     | `admin`     |
| utilisateur normal | `invite`    | `hello1234`  |

