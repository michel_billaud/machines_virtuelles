Hello,

Je suis Michel Billaud (mailto:michel.billaud@u-bordeaux.fr), et
j'enseigne (plus pour très longtemps) au Département Informatique de
l'IUT de Bordeaux.

Nos étudiants informaticiens ont besoin de faire, des manipulations sous
Linux. Normalement, ils peuvent le faire sur les machines en
libre-service, mais en ces temps de distanciel, ça ne le fait pas.

La plupart sont équipés d'ordinateurs, portables ou pas, sous Windows
ou MacOs, mais un certain nombre ne souhaitent pas faire un "Dual
Boot". Pour diverses raisons : peur de tout casser, ordinateur
familial, etc.


# Solution : machines virtuelles

La solution proposée, pour minimiser les complications
d'administration, est de **fournir une machine virtuelle Linux**.


Cette machine, administrée par l'étudiant

- comportera les logiciels de base préinstallés immédiatement nécessaires
aux débutants,
- sera disponible au téléchargement dans un format (.OVA) accepté par
les logiciels de virtualisation "standards" et gratuits VirtualBox et
VmWare.

**Suivi** : il sera certainement nécessaire, en cours d'année,
d'ajouter des logiciels.  Pour simplifier le travail d'administration

- les manipulations à effectuer seront décrites par des scripts
- l'utilisateur téléchargera et exécutera, comme administrateur, les
  scripts dont il a besoin
 
