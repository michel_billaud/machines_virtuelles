#
# Installation de Processing
#
# Installation dans répertoire /opt,
# ajout de l'entrée dans le menu graphique "Programmation"
# commande processing dans /usr/local/bin

# MB, version du 22 septembre 2020
#
# Voir https://processing.org/download/
# dernière version = 3.5.4 (janvier 2020) au 22/9/2020

# 1. téléchargement de l'archive dans /tmp

cd /tmp
wget https://download.processing.org/processing-3.5.4-linux64.tgz

# 2. désarchivage dans /opt

cd /opt
tar xzf /tmp/processing-3.5.4-linux64.tgz

# 3. installation

cd /opt/processing-3.5.4
./install.sh

# Pour pouvoir lancer facilement processing depuis la ligne de commande

ln -s /opt/processing-3.5.4/processing /usr/local/bin

# 4. ménage

rm  /tmp/processing-3.5.4-linux64.tgz
