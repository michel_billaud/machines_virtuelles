#

# Recette pour l'installation de Visual Studio Code
# création 2020-09-21

# 1. Ajout des clés du dépôt

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/

# 2. Ajout du dépôt

sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

# 3. Mise à jour de la liste des packages, Installation 

sudo apt-get update
sudo apt-get install code 

