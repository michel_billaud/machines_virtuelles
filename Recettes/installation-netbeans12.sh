#

# Installation de Netbeans 12
# dans /usr/local
# installation entrée menu Programmation

# Tests effectués (22/9/2020)
# - l'exemple "Anagramme" fonctionne.
# - projet maison : Java with Ant / Java Application / HelloWorld



# doc https://linuxhint.com/install_netbeans_ide_debian_10/

# installation du JDK 11.
sudo apt install openjdk-11-jdk


# téléchargement de l'installateur (374 Mo)

cd /tmp

wget http://apache.crihan.fr/dist/netbeans/netbeans/12.1/Apache-NetBeans-12.1-bin-linux-x64.sh

# installation (interactive)

cat <<EOF
INDICATIONS POUR INSTALLATION NETBEANS

- Au minimum,  BASE IDE + JAVA SE + HTML5/JavaScript (inst : 481,9 Mo)
- Choisir plateforme /usr/lib/jvm/java-1.11.0-openjdk-amd64
EOF

sh Apache-NetBeans-12.1-bin-linux-x64.sh

# customize :
# accepter

# téléchargement 650 Mo
# finish

# utilisation : projet Java avec ant
# L'utilisateur doit installer le plugin nb-javac
